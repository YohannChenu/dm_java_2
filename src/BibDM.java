import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){

        if (liste.isEmpty()) {
            return null;
        }
        Integer min=liste.get(0);
        for (int i=1;i<liste.size();i++){
            if (liste.get(i)<min)
                min=liste.get(i);
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        
        for (T elem : liste){
            if (elem.compareTo(valeur)<1){
                return false;
            }
        }
        return true;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        int i=0;
        int j=0;
        T elemFin=null;
        ArrayList<T> listeRes = new ArrayList<>();
        while (i<liste1.size() && j<liste2.size()){
            if (liste1.get(i).compareTo(liste2.get(j))>0){
                j++;
            }
            else if (liste1.get(i).compareTo(liste2.get(j))<0) 
                i++;
            else {
                if (liste1.get(i)!=elemFin){
                    listeRes.add(liste1.get(i));
                    elemFin=liste1.get(i);
                }
                i++;
                j++;
            }
        }
        return listeRes;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        String mot="";
        List<String> res= new ArrayList<>();
        for (int i=0;i<texte.length();i++){        
            if ((texte.charAt(i)==' ' && !mot.equals("")) || texte.length()==i+1){
                if (texte.length()==i+1 && texte.charAt(i)!=' '){
                    mot+=texte.charAt(i);
                }
                res.add(mot);
                mot="";
            }
            if (texte.charAt(i)!=' ')
                mot+=texte.charAt(i);
        }
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Map<String,Integer> mots = new HashMap<>();
        List<String> listeStr = decoupe(texte);
        for (String mot : listeStr){
            if (mots.containsKey(mot))
                mots.put(mot,mots.get(mot)+1);
            else 
                mots.put(mot,1);
        }
        if (mots.size()==0){
            return null;
        }
        String valMax=listeStr.get(0);
        Collections.sort(listeStr);
        for (String mot : listeStr){
            if (mots.get(valMax)<mots.get(mot))
                valMax = mot;               
            if (mots.get(mot).equals(mots.get(valMax))){
                ArrayList<String> listeDeux = new ArrayList<>();
                listeDeux.add(mot);
                listeDeux.add(valMax);
                valMax=Collections.min(listeDeux);
            }
        }
        return valMax;        
    }

// version sans HashMap mais qui prend trop de temps 
/*         String motMax=null;
        int cptMotMax=0;
        int cptMot=0;
        List<String> listeMot=decoupe(texte);
        for (int i=0;i<listeMot.size();i++){
            for (int j=0;j<listeMot.size();j++){
                if (listeMot.get(i).equals(listeMot.get(j))){
                    cptMot+=1;                  
                }
            if (cptMot>cptMotMax){
                cptMotMax=cptMot;
                motMax=listeMot.get(i);
            }
            if (cptMot==cptMotMax){
                int res=listeMot.get(i).compareTo(motMax);
                if (res<0){
                    motMax=listeMot.get(i);
                }
            }
            }
            cptMot=0;
        }
        return motMax;
    } */
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cptParG=0;
        int cptParD=0;
        if (chaine.length()!=0){
            if (chaine.charAt(0)==')' || chaine.charAt(chaine.length()-1)=='('){
                return false;
            }
        }
        for (int i=0;i<chaine.length();i++){
            if (chaine.charAt(i)=='('){
                cptParG+=1;
            }
            if (chaine.charAt(i)==')'){
                cptParD+=1;
            }
        }
        return cptParD==cptParG;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        List<Character> memoire = new ArrayList<>();
        for (int i=0;i<chaine.length();i++){
            if (chaine.charAt(i)=='(' || chaine.charAt(i)=='[') {
                memoire.add(chaine.charAt(i));
            }
            if (chaine.charAt(i)==')' ||  chaine.charAt(i)==']' ){
                if (memoire.size()==0){
                    return false;
                }
                else if ((chaine.charAt(i)==')' && memoire.get(memoire.size()-1)=='(') || (chaine.charAt(i)==']' && memoire.get(memoire.size()-1)=='['))
                        memoire.remove(memoire.size()-1);
                    else
                        return false;
            }
        }
        return memoire.size()==0;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.size()==0){
            return false; 
        }
        int fin=liste.size()-1;          
        int centre;
        int debut=0;    
        while (debut<fin){
            centre=((fin+debut)/2);
            if (valeur.compareTo(liste.get(centre))==0){ 
                return true; 
            }
            else if (valeur.compareTo(liste.get(centre))>0){
                debut=centre+1;
            }
            else {
                fin=centre;
            }
        } 
        return liste.get(fin).equals(valeur);  
    }
}
